
$(document).ready(function() {

    var toggleAffix = function(affixElement, scrollElement, wrapper) {
  
        var height = affixElement.outerHeight(),
            top = wrapper.offset().top;

        
        if (scrollElement.scrollTop() >= (height)){
            affixElement.addClass("affix");
            affixElement.removeClass("affix-top");
        }
        else {
            affixElement.addClass("affix-top");
            affixElement.removeClass("affix");
        }
      
    };
  

    $('[data-toggle="affix"]').each(function() {
        var ele = $(this),
            wrapper = $('<div></div>');
        
        ele.before(wrapper);
        $(window).on('scroll resize', function() {
            toggleAffix(ele, $(this), wrapper);
        });
        
        // init
        toggleAffix(ele, $(window), wrapper);
    });

    $('.panel-collapse').on('show.bs.collapse', function () {
        let parent = $(this).parent();
        parent.addClass("active");
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        let parent = $(this).parent();
        parent.removeClass("active");
    });

    $("section:odd").addClass("bg-color");

});